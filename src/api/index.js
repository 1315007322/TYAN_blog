import request from '../utils/request'

const Api = {
    getBlogList(params) {
        return request.get('/api/blog/list', {
            params
        })//
    },
    getBlogDetail(params) {
        return request.get('/api/blog/detail', {
            params
        })
    }



}

export default Api;